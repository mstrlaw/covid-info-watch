import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

import axios from 'axios'
import moment from 'moment'
import NProgress from 'nprogress'

Vue.use(Vuex)

NProgress.configure({ showSpinner: false });

export default new Vuex.Store({
  state: {
    selectedDate: moment().utc(),
    appLoading: true,
    articles: [],
    tags: [],
    tweets: [],
    clusterData: {},
    secondaryArticles: [],
    cluster: '',
  },
  mutations: {
    setAppLoading(state, isLoading) {
      state.appLoading = isLoading
    },
    setSelectedDate(state, date) {
      state.selectedDate = date
    },
    saveArticles(state, articles) {
      state.articles = articles
    },
    saveTags(state, tags) {
      state.tags = tags
    },
    savePrimaryCluster(state, clusterData) {
      state.clusterData = clusterData
    },
    saveSecondaryArticles(state, articles) {
      state.secondaryArticles = articles
    },
    saveClusterTerm(state, cluster) {
      state.cluster = cluster
    },
    saveTweets(state, tweets) {
      state.tweets = tweets
    }
  },
  actions: {
    // eslint-disable-next-line
    async GET_DATA({ commit, state }, options ) {
      NProgress.start();
      let url = `${process.env.VUE_APP_API_ENDPOINT}/api/topics/covid?day=${moment(state.selectedDate).utc().format('DD-MM-YYYY')}`

      if (typeof options !== 'undefined') {
        if (options.cluster) {
          url += `&cluster=${options.cluster}`
        }
      }
      const res = await axios.get(url)
      const { data } = res

      if (data.secondaryCluster !== null) {
        commit('saveSecondaryArticles', data.secondaryCluster)
      } else {
        commit('saveSecondaryArticles', [])
      }

      commit('savePrimaryCluster', data.cluster)
      commit('saveTags', data.cluster.themes)
      commit('saveArticles', data.articles)
      NProgress.done();
    },
    // eslint-disable-next-line
    // async GET_TWEETS({ commit }, queryParam) {
    //   const url = `${process.env.VUE_APP_API_ENDPOINT}/api/tweets/${queryParam}`
    //   const res = await axios.get(url)
    //   console.log(res.data);
    // },
  },
  modules: {
    preferences: {
      namespaced: true,
      state: {
        hasSeenTour: false,
      },
      mutations: {
        hasSeenTour(state) {
          state.hasSeenTour = true
        }
      }
    }
  },
  plugins: [new VuexPersistence({
    modules: ['preferences']
  }).plugin]
})
