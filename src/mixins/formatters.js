import moment from 'moment'
import { MAPPED_BIAS_LONGNAME } from '@/utilities'

export default {
  methods: {
    publishDate(date) {
      return moment(date).from(moment())
    },
    publishDateFromTimestamp(date) {
      return moment.unix(date).from(moment().utc())
    },
    formatDate(date, format) {
      return moment(date).format(format)
    },
    biasShortName(bias) {
      return MAPPED_BIAS_LONGNAME[bias].shortname
    },
    biasLongName(bias) {
      return MAPPED_BIAS_LONGNAME[bias].longname
    }
  }
}
