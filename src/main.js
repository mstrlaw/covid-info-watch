import Vue from 'vue'
import App from './App.vue'
import VTooltip from 'v-tooltip'
import VueMeta from 'vue-meta'
import VueTour from 'vue-tour'
import vmodal from 'vue-js-modal'
import router from './router'
import store from './store'
import './registerServiceWorker'

var mixpanel = require('mixpanel-browser');

mixpanel.init(process.env.VUE_APP_MIXPANEL_TOKEN);

import './styles/index.css';

Vue.config.productionTip = false

Vue.use(VTooltip)
Vue.use(VueMeta)
Vue.use(VueTour)
Vue.use(vmodal)

Vue.prototype.$mixpanel = mixpanel

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
